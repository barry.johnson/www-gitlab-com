---
layout: markdown_page
title: "GCP Partnership Working Group"
description: "Better together with GCP and GitLab"
canonical_path: "/company/team/structure/working-groups/gcp-partnership/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | 2023-08-01     |
| End Date        | TBD             |
| Slack           | `#wg_gcp-partnership` (only accessible from within the company) |
| Google Doc      | [GCP Working Group Google Doc](https://docs.google.com/document/d/1vGVxKYv0r808iLq3LyoyHg0tRNiT_vDrvr7RGJugg-c/edit?usp=sharing) (only accessible from within the company) |
| Issue Label     | `WorkingGroup::GCP Partnership` |

## Overview and goals

| Topic | GCP | GitLab |
| --- | --- | --- |
| CI/CD | David Jacobs | Jackie Porter |
| Artifact & Metadata | Rishi Mukhopadhyay | Tim Rizzi |
| Onboarding | Patrick Faucher | Omar Fernandez |
| Design/UX |  | Pedro Moreira da Silva |
